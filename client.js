const debug = require('debug')('ClipsClient:client')
const protobuf = require('protobufjs')
const root = protobuf.loadSync('bluetooth_messages.proto')
const crypto = require('crypto')
const ecdh = crypto.createECDH('prime256v1')

const requestClass = root.lookupType('links.common.comms.Request')
const responseClass = root.lookupType('links.common.comms.Response')


class Client {
  constructor (writeCharacteristic, indicateCharacteristic) {
    this.writeCharacteristic = writeCharacteristic
    indicateCharacteristic.on('data', this.readChunk.bind(this))
    indicateCharacteristic.subscribe()
    debug('subscribed to indicate characteristic')
    this.publicKey = ecdh.generateKeys()
    console.log({publicKey: this.publicKey.toString('hex')})
    this.clientNonce = crypto.randomBytes(16)
  }

  start () {
    debug('start')
    setTimeout(() => {
      this.sendRequest({
        publicQuery: {
          currentTimeMillisDeprecated: (new Date()).getTime(),
          protocolVersion: 26
        }
      })
    }, 1000)
  }

  readChunk (chunk) {
    debug('read chunk', chunk.toString('hex'))
    const length = chunk[0]
    const content = chunk.slice(1)
    const response = responseClass.decode(content)
    debug(response)

    if (response.publicQuery) {
      this.sendRequest({
        initiatePairing: {
          handshakeVersion: 1,
          pubKey: this.publicKey.slice(1)
        }
      })
    } else if (response.initiatePairing) {
      const { pubKey } = response.initiatePairing
      this.sharedSecret = ecdh.computeSecret(Buffer.concat([Buffer.from([0x04]), pubKey]))
      debug({pubKey: pubKey.toString('hex'), sharedSecret: this.sharedSecret.toString('hex')})
      this.sendRequest({
        initiateSecureConnection: {
          nonce: this.clientNonce,
          deviceId: 0
        }
      })
    }
  }

  sendRequest(obj) {
    const request = requestClass.create(obj)
    const bytes = requestClass.encode(request).finish()
    const header = Buffer.from([bytes.length])
    debug(request)
    this.send(Buffer.concat([header, bytes]))
  }

  send(message) {
    const MAX_CHARACTERISTIC_SIZE = 20
    var cursor = 0
    var end, chunk

    const sendChunk = () => {
      if (cursor < message.length) {
        end = Math.min(cursor + MAX_CHARACTERISTIC_SIZE, message.length)
        chunk = message.slice(cursor, end)
        cursor = end
        debug('send chunk', chunk.toString('hex'))
        this.writeCharacteristic.write(chunk, false, sendChunk)
      }
    }
    sendChunk()
  }
}

module.exports = Client
