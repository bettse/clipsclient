const noble = require('noble')
const debug = require('debug')('ClipsClient:index')
const Client = require('./client')

const clipsService = "00000003000310008000001a11000100"

let writeCharacteristic
let indicateCharacteristic

noble.on('stateChange', stateChange)

function stateChange (state) {
  if (state === 'poweredOn') {
    noble.startScanning([clipsService])
  } else {
    noble.stopScanning()
  }
}

noble.once('discover', function (peripheral) {
  const { advertisement } = peripheral
  debug('Found', advertisement.localName)
  noble.stopScanning()

  peripheral.once('disconnect', function () {
    debug('disconnected')
    if (indicateCharacteristic) {
      indicateCharacteristic.unsubscribe()
    }
    stateChange('poweredOn') // restart scanning
  })

  peripheral.connect(function (error) {
    if (error) {
      debug(error)
      return
    }
    debug('connected')
    peripheral.discoverServices([clipsService], function (error, services) {
      if (error) {
        debug(error)
        return
      }

      services.forEach(function (service) {
        debug('service', service.uuid)
        if (service.uuid === clipsService) {
          service.discoverCharacteristics([], handleCharacteristics)
        }
      })
    })
  })
})

function handleCharacteristics (error, characteristics) {
  if (error) {
    debug(error)
    return
  }

  characteristics.forEach(function (characteristic) {
    // debug(`processing characteristic ${characteristic}`)
    if (characteristic.properties.includes('indicate')) {
      debug('found indicate characteristic', characteristic.uuid)
      indicateCharacteristic = characteristic
    }

    if (characteristic.properties.includes('write')) {
      debug('found write characteristic', characteristic.uuid)
      writeCharacteristic = characteristic
    }

    if (indicateCharacteristic && writeCharacteristic) {
      debug('Instantiating client')
      const client = new Client(writeCharacteristic, indicateCharacteristic)
      client.start()
    }
  })
}
